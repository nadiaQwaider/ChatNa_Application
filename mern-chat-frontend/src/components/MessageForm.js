import React, { useContext, useEffect, useRef, useState } from 'react'
import { Button, Col, Form, Row } from 'react-bootstrap';
import './MessageForm.css';
import { useSelector } from "react-redux";
import {AppContext} from '../context/appContext';

export default function MessageForm() {
  const user = useSelector((state) => state.user);
  const [message, setMessage] = useState("");
  const {socket, currentRoom, messages, setMessages, privateMemberMsg}= useContext(AppContext);
  const messageEndRef = useRef(null);
  useEffect(() => {
    scrollToBottom();
  },[messages]);
  function getFormattedDate(){
     const date = new Date();
     const year = date.getFullYear();
     let month = (1 + date.getMonth()).toString(); //the month function starts with zero
     month = month.length > 1 ? month : '0' + month; //if month 1->9 will put 0 in the left
     let day = date.getDate().toString();
     day = day.length > 1? day : "0" + day;
     return month + "/" + day + "/" + year;
  }

  const todayDate = getFormattedDate();

  socket.off("room-messages").on("room-messages", (roomMessages) =>{
    console.log("room messages" , roomMessages);
    setMessages(roomMessages);
  })

  function handleSubmit(e){
    e.preventDefault();
    if(!message) return; // if there is no message don't send anything
    const today = new Date();
    const minutes = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes() ;
    const hours = today.getHours();
    const time = hours + ":" + minutes;
    const roomId = currentRoom;
    socket.emit('message-room', roomId, message, user, time, todayDate);//send message to room
    setMessage("");// after send the message let it empty to send new one
  }

  function scrollToBottom(){
    messageEndRef.current?.scrollIntoView({behavior: 'smooth'});
  }
  return (
    <>
        <div className='messages-output'>

          {user && !privateMemberMsg?._id && <div className='alert alert-info' style={{textAlign: 'center'}}> You are in the {currentRoom} Room</div>}
          {user && privateMemberMsg?._id && <div className='alert alert-info conversation-info'> Your Conversation With {privateMemberMsg.name} <img src={privateMemberMsg.picture} style={{width: 60, height:60,borderRadius:'50%'}}/></div>}

          {!user && <div className='alert alert-danger'>Please Login</div>}

          {user && 
              messages.map(({_id:date,messagesByDate}, idx) => (
                <div key={idx}> 
                    <p className='alert alert-info text-center message-date-indicator'>{date}</p>
                    {messagesByDate?.map(({content, time, from:sender}, msgIdx) => (
                    <div className={sender?.email == user?.email ? "message" : "incoming-message"} key={msgIdx}>
                         <div className='message-inner'>
                             <div className='d-flex align-items-center mb-3'>
                                 <img src={sender.picture} style={{width: 35, height: 35, objectFit:'cover', borderRadius: '50%', marginRight: 10}}/>
                                 <p className='message-sender'>{sender._id == user?._id ? "You" : sender.name}</p>
                             </div>
                             <p className='message-content'>{content}</p>
                             <p className='message-timestamp-left'>{time}</p>
                         </div>
                    </div>
                    ))}  
                </div>
              ))}
              <div ref ={messageEndRef}></div>
        </div>
        <Form onSubmit={handleSubmit} >
                <Row>
                    <Col md={10}>
                        <Form.Group>
                            <Form.Control type='text' placeholder='Enter Your Message' disabled={!user} value={message} onChange={(e) => setMessage(e.target.value)}></Form.Control>
                        </Form.Group>
                    </Col>
                    <Col md={2}>
                            <Button type='submit' style={{ width: "100%", backgroundColor: "#5FBDFF" ,alignContent:"center"}} disabled={!user}>
                                <i className='fas fa-paper-plane' style={{marginRight:"5px"}}></i>
                            </Button>
                    </Col>
                </Row>
            </Form>
    </>
    
  )
}
