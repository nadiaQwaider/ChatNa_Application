import React, { useContext, useEffect } from 'react'
import { Col, ListGroup, Row } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux';
import { AppContext } from '../context/appContext';
import {addNotifications, resetNotifications} from'../features/userSlice';
import './Sidebar.css';
export default function Sidebar() {
    const user = useSelector((state) => state.user);
    //useDispatch() allowed us to call notifications
    const dispatch = useDispatch();
    const { socket, rooms, setRooms,  setCurrentRoom, members, setMembers, currentRoom,  setPrivateMemberMsg, privateMemberMsg, messages, setMessages} = useContext(AppContext);//
    socket.off("new-user").on("new-user", (payload) => {
      setMembers (payload);
    });
  useEffect(() => {
    if (user) {
      setCurrentRoom("General");
      getRooms();
      socket.emit("new-user");
      socket.emit("join-room", "General");
    }
  }, [])

  function getRooms(){
    fetch("http://localhost:5001/rooms").then((res) => res.json()).then ((data) => setRooms(data));
  }

  function joinRoom(room, isPublic = true){
    if(!user){
      return alert ("Please login");
    }

    socket.emit("join-room", room, currentRoom);
    setCurrentRoom(room);
    if (isPublic){
      setPrivateMemberMsg(null);
    }
    //dispatch for notifications
    //once you open the room conversation will delete the notification
    dispatch(resetNotifications(room));
  }
  socket.off("notifications").on("notifications", (room) => {
    if (currentRoom != room) dispatch(addNotifications(room));
  });
function orderIds(id1, id2) {
  if(id1 > id2) {
    return id1 + "-" + id2;
  } else {
    return id2 + "-" + id1;
  }
}

function handlePrivateMemberMsg(member){
  setPrivateMemberMsg(member);
  const roomId = orderIds(user._id, member._id);
  joinRoom(roomId, false);
}

  if(!user) {
      return <></>
    }
  return (
    <>
    <h2>Available Rooms</h2>
    <ListGroup>
        {rooms.map((room, idx) => (
            <ListGroup.Item key={idx} onClick={() => joinRoom(room) } active= {room == currentRoom} style={{cursor:'pointer', display:'flex', justifyContent:'space-between'}} >
              {room} { currentRoom !== room && <span className='badge rounded-pill bg-primary'>{user.newMessages[room]}</span> }
            </ListGroup.Item>
        ))}
    </ListGroup>
    <h2>Members</h2>
    <ListGroup>
    { members.map((member,id) => (
        <ListGroup.Item key={id} style={{cursor: "pointer"}} active= {privateMemberMsg?._id == member?._id} disabled= {member?._id == user?._id} onClick={() => handlePrivateMemberMsg(member)}>
          <Row>
            <Col xs={2} className='member-status'>
              <img src={member.picture} className='member-status-img'/>
              {member.status === "online" ? <i className='fas fa-circle sidebar-online-status'></i> : <i className='fas fa-circle sidebar-offline-status'></i> }
            </Col>
            <Col xs={8}>
              {member.name}
              {member._id === user?._id && " (You) "}
              {member.status === "offline"  && " (Offline) "}
            </Col>
            <Col xs={2}>
            <span className='badge rounded-pill bg-primary'>{user.newMessages[orderIds(member._id, user._id)]}</span>
            </Col>
          </Row>
    </ListGroup.Item>
        ))}

    </ListGroup>
    </>
  )
}
