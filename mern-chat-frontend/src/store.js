// this code to store our data, so we dont need to refresh the page everytime (realtime)
import { configureStore } from "@reduxjs/toolkit";
import userSlice from "./features/userSlice";
import appApi from "./services/appApi";
import { thunk } from "redux-thunk";
import storage from "redux-persist/lib/storage";
import { combineReducers } from "redux";
import persistReducer from "redux-persist/es/persistReducer";

//reducers (where will store our logic states)
//combineReducers takes many reducers and return new one
const reducer = combineReducers({
    user: userSlice,
    [appApi.reducerPath]:appApi.reducer,
});
//persist our store (persist takes root reducer and config it then gives enhanced reducer)
const persistConfig = {
    key: 'root',
    storage,
    blackList: [appApi.reducerPath],
};

const persistedReducer = persistReducer(persistConfig, reducer);

//creating the store
const store = configureStore({
    reducer: persistedReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({serializableCheck: false}).concat(thunk, appApi.middleware),
});

export default store;