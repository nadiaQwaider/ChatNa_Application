import React, { useState } from 'react';
import { Row, Col, Button, Container } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { Link, useNavigate } from 'react-router-dom';
import"./Signup.css";
import Robot from "../assets/robot.jpg";
import {useSignupUserMutation} from '../services/appApi';

export default function Signup() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [Image, setImage] = useState(null);
    const [uploadingImage, setuploadingImage] = useState(false);
    const [ImagePreview, setImagePreview] = useState(null);
    const [signupUser, {isLoading, error}] = useSignupUserMutation();
    const Navigate = useNavigate();
    function validateImg(e){
     const file = e.target.files[0];
     if(file.size >= 1048567) {
        return alert ("Maximum File Size is 1 MB")
     }else {
        setImage(file);
        setImagePreview(URL.createObjectURL(file));
     }
    }

    async function uploadImage() {
        const data = new FormData();
        data.append('file', Image);
        data.append('upload_preset','h9fmq3tq'); //the preset h9fmq3tq from my cloudinary
        try{
            setuploadingImage(true);
            let res = await fetch('https://api.cloudinary.com/v1_1/dsbquensb/image/upload', { //dsbquensb is my cloud name on cloudinary.com 
            method:'post', //post image
            body: data,
        });
        const urlData = await res.json();
        setuploadingImage(false);
        return urlData.url;
        } catch (error) {
            setuploadingImage(false);
            console.log(error);
        }
    }
    
    async function handleSignup(e){
    e.preventDefault();
    if(!Image) return alert ("Please Upload Your Profile Picture");
    const url = await uploadImage(Image);
    console.log(url);
    //signup the user
    signupUser({name, email, password, picture: url }).then(({data}) => {
        if(data) {
            console.log(data);
            Navigate('/chat');
        }
    })
    }

  return (
    <Container>
        <Row>
            <Col md={5} className="signup__bg"></Col>

            <Col md={7} className="d-flex align-items-center justify-content-center flex-direction-column">
                <Form style={{ width: "80%", maxWidth: 500 }} onSubmit={handleSignup}>
                  <h1 className='text-center'>Create Account</h1>
                  <div className='signup-profile-pic_container'>
                     <img src={ImagePreview || Robot} className='signup-profile-pic' alt='signup-profile-pic'/>
                     <label htmlFor='image-upload' className='image-upload-label'>
                        <i className='fas fa-plus-circle add-picture-icon'></i>
                     </label>
                     <input type='file' id='image-upload' hidden accept='image/png, image/jpeg' onChange={validateImg}/>
                  </div>

                  <Form.Group className="mb-3" controlId="formBasicName">
                  {error && <p className='alert alert-danger'>{error.data}</p>}
                        <Form.Label>Name</Form.Label>
                        <Form.Control type="text" placeholder="Enter Your Name"  onChange={(e) => setName(e.target.value)} value={name} autoComplete='username'/>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email"  onChange={(e) => setEmail(e.target.value)} value={email} autoComplete='email'/>
                        <Form.Text className='text-muted'>We'll never share your email with anyone else.</Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password"  onChange={(e) => setPassword(e.target.value)} value={password} autoComplete='current-password'/>
                    </Form.Group>

                    <Button variant='primary' type='submit'>
                        {uploadingImage || isLoading ? 'Signing you up...' : 'Sign Up'}
                    </Button>

                    <div className="py-4">
                         <p className='text-center'>You have an account? <Link to="/login">Login</Link></p>
                    </div>

                </Form>
            </Col>
        </Row>
    </Container>  )
}
