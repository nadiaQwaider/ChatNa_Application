import React from 'react';
import { Row, Col, Button} from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import "./Home.css";
export default function Home() {
  return (
    <Row>
      <Col md={6} className="d-flex fles-direction-column align-items-center justify-content-center mt-5">
        <div className='home-content'>
            <h1 className='mb-3'>Share the world with your freinds</h1>
            <h3><span style={{color: "#2D9596", fontFamily: "fantasy", fontWeight:'800', textAlign:'center'}}>CHATNA</span> Will Let You Do!</h3>
            <LinkContainer to="/signup">
                <Button className='get-started mt-3 mb-3'>
                    Get Started <i className='fas fa-message home-message-icon'></i>
                </Button>
            </LinkContainer>
        </div>
      </Col>
      <Col md={6} className='home__bg '></Col>
    </Row>
  )
}
