const express = require ('express');
const cors = require ('cors');
const app = express();
const userRoutes = require('./routes/userRoutes');
const User = require ('./models/User');
const Message = require ('./models/Message');
const rooms = ['General', 'Tech', 'Finance', 'Crypto'];
app.use(express.urlencoded({extended: true}));
app.use(express.json());
app.use(cors());
app.use('/users', userRoutes);
require('./connection');
const server = require('http').createServer(app);
const PORT = 5001;
const io = require('socket.io')(server, {
    cors: {
        origin: 'http://localhost:3000',
        methods: ['GET', 'POST']
    }
})



app.delete('/logout', async(req,res) => {
    try{
        const{_id, newMessages} = req.body;
        const user = await User.findById(_id);
        user.status = "offline"; // change the user status
        user.newMessages = newMessages; // update user messages (chat)
        await user.save();
        const members = await User.find(); //update members list (online)
        //socket.broadcast.emit('new-user', members); //share member list with users
        res.status(200).send();
    } catch (e){
        console.log(e);
        res.status(400).send();
    }
})

app.get('/rooms', (req,res) => {
    res.json(rooms)
})

//get Last Messages From Room (group)
async function getLastMessagesFromRoom(room){
    let roomMessages = await Message.aggregate([
        {$match: {to: room}},
        // group the messages from this room By Date
        {$group: {_id: '$date', messagesByDate: {$push: '$$ROOT'}}}
    ])
    return roomMessages;
}

//03/06/2024 => 20240603
//month/day/year => YearMonthDay
function sortRoomMessagesByDate(messages){
    return messages.sort(function(a,b){
        let date1 = a._id.split('/');
        let date2 = b._id.split('/');
        date1 = date1[2]+date1[0]+date1[1];
        date2 = date2[2]+date2[0]+date2[1];
        return date1 < date2 ? -1 : 1;
    })
}

// socket connection
io.on('connection', (socket) => {

     // once we have (new user) we will update the room users to inform users //new-user is event
     socket.on('new-user', async() => {
        const members = await User.find();
        io.emit('new-user', members);// io.emit will return all users connected with socket
     })

    //once user (join room) we will update and view the messages in the room sorted by date //join-room is event
    socket.on('join-room', async(newRoom,previousRoom) => {
        socket.join(newRoom);
        socket.leave(previousRoom)
        let roomMessages = await getLastMessagesFromRoom(newRoom);
        roomMessages = sortRoomMessagesByDate(roomMessages);
        socket.emit('room-messages', roomMessages)
    })

    //once user send message in the room
    socket.on('message-room', async(room, content, sender, time, date) => {
        console.log("new messages " + content);
        const newMessage = await Message.create({content, from: sender, time, date, to: room});
        let roomMessages = await getLastMessagesFromRoom(room);
        roomMessages = sortRoomMessagesByDate(roomMessages);
        io.to(room).emit('room-messages', roomMessages);
        socket.broadcast.emit('notifications', room)
    })

})

server.listen(PORT, ()=> {
    console.log('listening to port', PORT)
})