import React ,{ useContext, useState } from 'react'
import { Row, Col, Button, Container, Spinner } from 'react-bootstrap';
import Form from 'react-bootstrap/Form';
import { Link, useNavigate } from 'react-router-dom';
import "./Login.css";
import {useLoginUserMutation} from '../services/appApi';
import { AppContext } from '../context/appContext';

export default function Login() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [loginUser, {isLoading, error}] = useLoginUserMutation();
    const Navigate = useNavigate();
    const {socket} = useContext(AppContext);
     function handleLogin(e){
        e.preventDefault();
        loginUser({email, password}).then(({data}) => {
            if(data) {
                //socket work => add the new user
                socket.emit('new-user');
                // navigate to the chat
                Navigate('/chat');
            }
        })
    }
  return (
    <Container>
        <Row>
            <Col md={5} className="login__bg"></Col>
            <Col md={7} className="d-flex align-items-center justify-content-center flex-direction-column mt-2">
                <Form style={{ width: "80%", maxWidth: 500 }} onSubmit={handleLogin}>
                    <Form.Group className="mb-3" controlId="formGroupEmail">
                        {error && <p className='alert alert-danger'>{error.data}</p>}
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email"  onChange={(e) => setEmail(e.target.value)} value={email} autoComplete='email' required/>
                        <Form.Text className='text-muted'>We'll never share your email with anyone else.</Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="formGroupPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password"  onChange={(e) => setPassword(e.target.value)} value={password} autoComplete='current-password' required/>
                    </Form.Group>
                    <Button variant='success' type='submit'>
                        { isLoading ? <Spinner animation='grow'/> : "Login"}
                    </Button>
                    <div className="py-4">
                         <p className='text-center'>Don't have an account? <Link to="/signup">Signup</Link></p>
                    </div>
                </Form>
            </Col>
        </Row>
    </Container>

  );
}
