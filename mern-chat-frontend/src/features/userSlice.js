import { createSlice } from '@reduxjs/toolkit';
import appApi from '../services/appApi';

export const userSlice = createSlice({
    name: "user",
    initialState: null,
    reducers: {
        // addNotifications will give me Notification if there is new messages from group or member
        addNotifications: (state, {payload}) => {
            if(state.newMessages[payload]){
                //everytime you will receive new message from group or member the Notifications counter will increase
                state.newMessages[payload] = state.newMessages[payload] + 1;
            }else {
                state.newMessages[payload] = 1;
            }
        },
        //resetNotifications will delete the Notifications counter
        resetNotifications: (state, {payload}) => {
            delete state.newMessages[payload];
        },
    },
    extraReducers: (builder) => {
        //save user after signup
        builder.addMatcher(appApi.endpoints.signupUser.matchFulfilled, (state, { payload }) => payload);

         //save user after login
         builder.addMatcher(appApi.endpoints.loginUser.matchFulfilled, (state, { payload }) => payload);

          //distroy user session
        builder.addMatcher(appApi.endpoints.logoutUser.matchFulfilled, () => null);
    }
});
//only actions change the user state
export const { addNotifications, resetNotifications} = userSlice.actions;
export default userSlice.reducer;