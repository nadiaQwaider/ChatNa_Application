import React from 'react';
import {Nav, Navbar, NavDropdown, Container, Button} from 'react-bootstrap';
import {LinkContainer} from 'react-router-bootstrap';
import logo from '../assets/logo.png';
import { useSelector } from 'react-redux';
import {useLogoutUserMutation} from '../services/appApi';

export default function Navigation() {
  const user = useSelector((state) => state.user);
  const [logoutUser] = useLogoutUserMutation();
  
  async function handleLogout(e){
    e.preventDefault();
    await logoutUser(user);
    window.location.replace("/");
  }

  return (

    <Navbar expand="lg" className="bg-body-tertiary">
      <Container>
        <LinkContainer to='/'>
           <Navbar.Brand >
            <img src={logo} style={{ width: 110, height: 110, borderRadius:"50%"}} alt='logo'/>
           </Navbar.Brand>
        </LinkContainer>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ms-auto">

            {!user && ( 
             <LinkContainer to='/login'>
                <Nav.Link>Login</Nav.Link>
             </LinkContainer>
            )}
             <LinkContainer to='/chat'>
                <Nav.Link>Chat</Nav.Link>
             </LinkContainer>
             {user && ( 
            <NavDropdown title={
                                <> 
                                  <img src={user.picture} style={{ width: 30, height: 30, marginRight:10, objectFit:"cover", borderRadius: "50%"}} alt='user-pic'/>
                                  {user.name}
                                </>
                                }
            >
              <NavDropdown.Item href="#action/3.1">Profile</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
              <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
              <NavDropdown.Divider />
             
              <NavDropdown.Item > <Button onClick={handleLogout} variant="danger" >Logout</Button> </NavDropdown.Item>
            </NavDropdown>
                        )}

          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}
